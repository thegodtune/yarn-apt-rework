/* eslint-disable */
module.exports = {
name: "@yarnpkg/plugin-apt",
factory: function (require) {
var plugin;(()=>{"use strict";var e={d:(n,o)=>{for(var a in o)e.o(o,a)&&!e.o(n,a)&&Object.defineProperty(n,a,{enumerable:!0,get:o[a]})},o:(e,n)=>Object.prototype.hasOwnProperty.call(e,n),r:e=>{"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})}},n={};e.r(n),e.d(n,{default:()=>t});const o=require("clipanion"),a=require("@yarnpkg/cli");class t extends a.BaseCommand{constructor(){super(...arguments),this.local=o.Option.Boolean("-l, --local",!1,{description:"Install from local cache"}),this.resolve=o.Option.Boolean("-r, --resolve",!1,{description:""})}async execute(){}}t.paths=[["apt"]],t.usage=o.Command.Usage({description:"\n    ------------------------------------------\n\n    Yarn2 apt module resolve plugin for debian\n\n    ------------------------------------------\n\n    ",details:"\n    The apt command check if a node module is installed via apt, \n\n    if true resolves and links the module as a project dependency\n\n    for the nodejs project within which the command was ran.\n    ",examples:[["Resolve memfs package\n","yarn apt --resolve memfs\n"]]}),plugin=n})();
return plugin;
}
};