import {CommandContext, Plugin} from '@yarnpkg/core';
import {Command, Usage, UsageError, Option} from "clipanion";
import { BaseCommand } from "@yarnpkg/cli";
import { xfs } from "@yarnpkg/fslib";

export default class Apt extends BaseCommand {

  static paths = [
    [`apt`],
  ];

  static usage: Usage = Command.Usage({
    description: `
    ------------------------------------------\n
    Yarn2 apt module resolve plugin for debian\n
    ------------------------------------------\n
    `,
    details: `
    The apt command check if a node module is installed via apt, \n
    if true resolves and links the module as a project dependency\n
    for the nodejs project within which the command was ran.
    `,
    examples: [[
            `Resolve memfs package\n`,
            `yarn apt --resolve memfs\n`
        ]]
  })

  local = Option.Boolean(`-l, --local`, false, {
    description: `Install from local cache`
  })
  resolve = Option.Boolean(`-r, --resolve`, false, {
    description: ``
  })
  async execute() {

  }
}

