"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const clipanion_1 = require("clipanion");
const cli_1 = require("@yarnpkg/cli");
class Apt extends cli_1.BaseCommand {
    constructor() {
        super(...arguments);
        this.local = clipanion_1.Option.Boolean(`-l, --local`, false, {
            description: `Install from local cache`
        });
        this.resolve = clipanion_1.Option.Boolean(`-r, --resolve`, false, {
            description: ``
        });
    }
    async execute() {
    }
}
exports.default = Apt;
Apt.paths = [
    [`apt`],
];
Apt.usage = clipanion_1.Command.Usage({
    description: `
    ------------------------------------------\n
    Yarn2 apt module resolve plugin for debian\n
    ------------------------------------------\n
    `,
    details: `
    The apt command check if a node module is installed via apt, \n
    if true resolves and links the module as a project dependency\n
    for the nodejs project within which the command was ran.
    `,
    examples: [[
            `Resolve memfs package\n`,
            `yarn apt --resolve memfs\n`
        ]]
});
